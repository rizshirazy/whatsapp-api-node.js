const { createLogger, format, transports, transport } = require("winston");
const { combine, timestamp, label, printf, colorize } = format;

const logFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

const log = createLogger({
  level: "info",
  format: combine(
    label({ label: "WA API" }),
    timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
    logFormat
  ),
  transports: [
    new transports.Console({
      format: combine(
        timestamp({ format: "YYYY-MM-DD HH:mm:ss" }),
        colorize(),
        logFormat
      ),
    }),
    new transports.File({
      filename: "logs/app.log",
      datePattern: "YYYY-MM-DD",
      maxsize: "10m",
    }),
    new transports.File({
      filename: "logs/error.log",
      datePattern: "YYYY-MM-DD",
      maxsize: "10m",
      level: "error",
    }),
  ],
});

module.exports = { log };
