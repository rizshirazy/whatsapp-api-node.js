const phoneNumberFormatter = (number) => {
  // Filter number only
  let formatted = number.replace(/\D/g, "");

  // Replace leading zero with country code
  if (formatted.startsWith("0")) {
    formatted = "62" + formatted.substr(1);
  }

  // Add WhatsApp Number format
  formatted += "@c.us";

  return formatted;
};

module.exports = {
  phoneNumberFormatter,
};
