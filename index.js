const { Client } = require("whatsapp-web.js");
const express = require("express");
const { body, validationResult } = require("express-validator");
const qrcode = require("qrcode");
const socketIO = require("socket.io");
const http = require("http");
const fs = require("fs");
const { phoneNumberFormatter } = require("./helpers/formatter");
const { log } = require("./helpers/logger");

const PORT = 3000;
const SESSION_FILE_PATH = "./session.json";

let CLIENT_CONNECTED = false;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let sessionCfg;
if (fs.existsSync(SESSION_FILE_PATH)) {
  sessionCfg = require(SESSION_FILE_PATH);
}

const client = new Client({
  puppeteer: {
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-dev-shm-usage",
      "--disable-accelerated-2d-canvas",
      "--no-first-run",
      "--no-zygote",
      "--single-process", // <- this one doesn't works in Windows
      "--disable-gpu",
    ],
  },
  session: sessionCfg,
});

const checkRegisteredNumber = async (number) => {
  const isRegistered = await client.isRegisteredUser(number);
  return isRegistered;
};

const findGroupByName = async function (name) {
  const group = await client.getChats().then((chats) => {
    return chats.find(
      (chat) => chat.isGroup && chat.name.toLowerCase() == name.toLowerCase()
    );
  });
  return group;
};

app.get("/", (req, res) => {
  res.sendFile("index.html", { root: __dirname });
});

app.get("/loading", (req, res) => {
  res.sendFile("spinner.gif", { root: __dirname });
});

app.get("/ready", (req, res) => {
  res.sendFile("logo.png", { root: __dirname });
});

client.on("message", (msg) => {
  if (msg.body == "!ping") {
    msg.reply("pong");
  } else if (msg.body == "!groups") {
    client.getChats().then((chats) => {
      const groups = chats.filter((chat) => chat.isGroup);

      if (groups.length == 0) {
        msg.reply("You have no group yet.");
      } else {
        let replyMsg = "*YOUR GROUPS*\n\n";
        groups.forEach((group, i) => {
          replyMsg += `ID: ${group.id._serialized}\nName: ${group.name}\n\n`;
        });
        replyMsg +=
          "_You can use the group id to send a message to the group._";
        msg.reply(replyMsg);
      }
    });
  }
});

client.initialize();

// SOCKET IO
io.on("connection", function (socket) {
  socket.emit("message", "Connecting...");

  // Generate and scan this code with your phone
  client.on("qr", (qr) => {
    log.info("QR RECEIVED", { message: qr });
    qrcode.toDataURL(qr, (err, url) => {
      socket.emit("qr", url);
      socket.emit("message", "QR Code received");
    });
  });

  client.on("ready", () => {
    log.info("WhatsApp is ready!");
    socket.emit("message", "WhatsApp is ready!");
    socket.emit("ready", "WhatsApp is ready!");
    CLIENT_CONNECTED = true;
  });

  client.on("authenticated", (session) => {
    log.info("AUTHENTICATED", { message: JSON.stringify(session, " ", 2) });
    CLIENT_CONNECTED = true;

    socket.emit("message", "WhatsApp is authenticated");
    socket.emit("authenticated", "WhatsApp is authenticated");
    sessionCfg = session;
    fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
      if (err) {
        console.error(err);
      }
    });
  });

  client.on("auth_failure", (session) => {
    // Fired if session restore was unsuccessfull
    log.error("AUTHENTICATION FAILURE", {
      message: "Auth failure, restarting...",
    });
    socket.emit("message", "Auth failure, restarting...");
  });

  client.on("disconnected", (reason) => {
    log.info("DISCONECTED", { message: "Whatsapp is disconnected!" });
    socket.emit("message", "Whatsapp is disconnected!");
    fs.unlinkSync(SESSION_FILE_PATH, function (err) {
      log.info("DISCONECTED", { message: "Session file destroyed!" });
      if (err) return console.log(err);
    });
    client.destroy();
    client.initialize();
    CLIENT_CONNECTED = false;
  });

  app.get("/status", (req, res) => {
    if (CLIENT_CONNECTED) {
      socket.emit("ready", "WhatsApp is authenticated");
    } else {
      socket.emit("message", "Whatsapp is disconnected!");
      socket.emit("logout", "Whatsapp is disconnected!");
    }

    res.status(200).json({
      connection_status: CLIENT_CONNECTED,
    });
  });
});

app.post(
  "/send-message",
  [body("number").notEmpty(), body("message").notEmpty()],
  async (req, res) => {
    const errors = validationResult(req).formatWith(({ msg }) => {
      return msg;
    });

    if (!errors.isEmpty()) {
      log.error("Unprocessable Entity", {
        message: JSON.stringify(errors.mapped),
      });

      return res.status(422).json({
        status: false,
        response: errors.mapped(),
      });
    }

    const number = phoneNumberFormatter(req.body.number);
    const message = req.body.message;

    // const isRegisteredNumber = await checkRegisteredNumber(number);
    // if (!isRegisteredNumber) {
    //   log.error("The number was not registered", {
    //     message: `${number}`,
    //   });

    //   return res.status(422).json({
    //     status: false,
    //     response: "The number was not registered",
    //   });
    // }

    client
      .sendMessage(number, message)
      .then((response) => {
        log.info("MESSAGE SENT", {
          message: `${number} ${message} ${JSON.stringify(response)}`,
        });

        res.status(200).json({
          status: true,
          response: response,
        });
      })
      .catch((err) => {
        log.error("MESSAGE SEND FAILED", {
          message: `${number} ${message} ${JSON.stringify(err)}`,
        });

        res.status(500).json({
          status: false,
          response: err,
        });
      });
  }
);

// Send message to group
// You can use chatID or group name, yea!
app.post(
  "/send-group-message",
  [
    body("id").custom((value, { req }) => {
      if (!value && !req.body.name) {
        throw new Error("Invalid value, you can use `id` or `name`");
      }
      return true;
    }),
    body("message").notEmpty(),
  ],
  async (req, res) => {
    const errors = validationResult(req).formatWith(({ msg }) => {
      return msg;
    });

    if (!errors.isEmpty()) {
      log.error("Unprocessable Entity", {
        message: JSON.stringify(errors.mapped),
      });

      return res.status(422).json({
        status: false,
        message: errors.mapped(),
      });
    }

    let chatId = req.body.id;
    const groupName = req.body.name;
    const message = req.body.message;

    // Find the group by name
    if (!chatId) {
      const group = await findGroupByName(groupName);
      if (!group) {
        log.error("Unprocessable Entity", {
          message: `No group found with name: ${groupName}`,
        });

        return res.status(422).json({
          status: false,
          message: "No group found with name: " + groupName,
        });
      }
      chatId = group.id._serialized;
    }

    client
      .sendMessage(chatId, message)
      .then((response) => {
        log.info("MESSAGE SENT", {
          message: `${chatId} ${message} ${JSON.stringify(response)}`,
        });

        res.status(200).json({
          status: true,
          response: response,
        });
      })
      .catch((err) => {
        log.error("MESSAGE SEND FAILED", {
          message: `${chatId} ${message} ${JSON.stringify(err)}`,
        });

        res.status(500).json({
          status: false,
          response: err,
        });
      });
  }
);

server.listen(PORT, function () {
  console.log(`App running on port ${PORT}`);
});
